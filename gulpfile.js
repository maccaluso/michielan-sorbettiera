'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var electron = require('electron-connect').server.create({
  stopOnClose: true
});

var callback = function(electronProcState) {
  console.log('electron process state: ' + electronProcState);
  if (electronProcState == 'stopped') {
    process.exit();
  }
};

gulp.task('sass', function () {
  var stream = gulp.src('./app/styles/sass/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./app/css/'));

  stream.on('end', function() {
    electron.reload( callback );
  });
});

gulp.task('serve', function () {

  // Start browser process
  electron.start(callback);

  // Restart browser process
  gulp.watch('main.js', electron.restart);

  // Reload renderer process
  gulp.watch([
    'app/**/*.html', 
    'app/**/*.template.html', 
    'app/**/*.js', 
    'app/**/*.component.js',
    'app/**/*.module.js'
  ], electron.reload);

  gulp.watch('app/styles/sass/**/*.scss', ['sass'], function(){

  })
});