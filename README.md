# Michielan Museum - Sorbettiera Test Application

#### License [CC0 (Public Domain)](LICENSE.md)

##INSTALLATION

Open a terminal and, if not installed, install [node.js + npm](https://nodejs.org/), [electron](https://electron.atom.io) and [electron-packager](https://github.com/electron-userland/electron-packager).
Then cd into the root of the cloned repo and type `electron-packager ./`

This should compile an executable for the current platform.
You can also run it without compiling with `npm start` or `npm run startdev` from the root.

##USAGE

The app should start windowed. `Cmd or Ctrl + F` set the app fullscreen. `ESC` to exit fullscreen.
`Cmd or Ctrl + R` reloads and `Cmd or Ctrl + Q` quits.

##OSC

The app listens to UDP port 57121 for receiving OSC messages.
Accepts simple OSC messages with address *"/rolling"* and *"/not-rolling"* and prints a string on screen accordingly.
The former acts as a continuos input. While the handle rolls it should ideally send *"/rolling"* continuously.
The latter acts like an interrupt. When the handle stops rolling sends a single *"/not-rolling"* message.

##PROCESSING

If needed the repo also contains a simple P5 application for testing communication. Clicking the canvas sends *"/rolling"*. Releasing mouse button sends *"/not-rolling"*.

##

######Not tested over a network but shuld work fine since it listens to *localhost*