const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const shell = electron.shell
const globalShortcut = electron.globalShortcut

let client
let mainWindow

let windowSettings = {
  x: 20,
  y: 40,
  width: 600,
  height: 800,
  frame: true
}

if( process.env.NODE_ENV === 'dev' )
{
  client = require('electron-connect').client
}

function createWindow () {
  mainWindow = new BrowserWindow( windowSettings )

  mainWindow.loadURL(`file://${__dirname}/app/index.html`)

  if( process.env.NODE_ENV === 'dev' )
  {
    mainWindow.openDevTools()
  }
  
  mainWindow.on('closed', function () { mainWindow = null })

  mainWindow.focus()
}

app.on('ready', () => {
  createWindow()

  if( process.env.NODE_ENV === 'dev' )
  {
    client.create( mainWindow, () => {
      console.log('main - electron-connect client created')
    })
  }

  const ret = globalShortcut.register('CommandOrControl+F', () => {
    mainWindow.setFullScreen(true)
  })

  const esc = globalShortcut.register('Escape', () => {
    mainWindow.setFullScreen(false)
  })

  const reload = globalShortcut.register('CommandOrControl+R', () => {
    mainWindow.reload()
  })
})

app.on('will-quit', () => { globalShortcut.unregisterAll() })

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})