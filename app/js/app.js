const $ = require('jquery')
const osc = require('node-osc')
// const THREE = require('three')
// const CANNON = require('cannon')

/**
* A container filled with spheres.
*/

var demo = new CANNON.Demo();
var nx=4, ny=4, nz=30;

demo.addScene((nx*ny*nz)+" spheres", function(){   createContainer(demo,nx,ny,nz);   });
demo.start();

function createContainer(demo,nx,ny,nz){

    // Create world
    var world = demo.getWorld();
    world.broadphase = new CANNON.SAPBroadphase(world); // Buggy?

    // Tweak contact properties.
    world.defaultContactMaterial.contactEquationStiffness = 1e11; // Contact stiffness - use to make softer/harder contacts
    world.defaultContactMaterial.contactEquationRelaxation = 2; // Stabilization time in number of timesteps

    // Max solver iterations: Use more for better force propagation, but keep in mind that it's not very computationally cheap!
    world.solver.iterations = 2;

    world.gravity.set(0,0,-10);

    // Since we have many bodies and they don't move very much, we can use the less accurate quaternion normalization
    world.quatNormalizeFast = true;
    world.quatNormalizeSkip = 16; // ...and we do not have to normalize every step.

    // Materials
    var stone = new CANNON.Material('stone');
    var stone_stone = new CANNON.ContactMaterial(stone, stone, {
      friction: 0.3,
      restitution: 0.2
    });
    world.addContactMaterial(stone_stone);

    var blade = new CANNON.Material('blade');
    var blade_blade = new CANNON.ContactMaterial(blade, blade, {
      friction: 10,
      restitution: 5
    });
    world.addContactMaterial(blade_blade);

    // ground plane
    var groundShape = new CANNON.Plane();
    var groundBody = new CANNON.Body({ mass: 0, material: stone });
    groundBody.addShape(groundShape);
    groundBody.position.set(0,0,-10);
    world.add(groundBody);
    demo.addVisual(groundBody);

    // plane -x
    var planeShapeXmin = new CANNON.Plane();
    var planeXmin = new CANNON.Body({ mass: 0, material: stone });
    planeXmin.addShape(planeShapeXmin);
    planeXmin.quaternion.setFromAxisAngle(new CANNON.Vec3(0,1,0), Math.PI/2);
    planeXmin.position.set(-10,0,0);
    world.add(planeXmin);
    // demo.addVisual(planeXmin);
    planeXmin.addEventListener("collide", function(e){ demo.collide(e.body.id); });

    // Plane +x
    var planeShapeXmax = new CANNON.Plane();
    var planeXmax = new CANNON.Body({ mass: 0, material: stone });
    planeXmax.addShape(planeShapeXmax);
    planeXmax.quaternion.setFromAxisAngle(new CANNON.Vec3(0,1,0), -Math.PI/2);
    planeXmax.position.set(10,0,0);
    world.add(planeXmax);
    // demo.addVisual(planeXmax);
    planeXmax.addEventListener("collide", function(e){ demo.collide(e.body.id); });

    // Plane -y
    var planeShapeYmin = new CANNON.Plane();
    var planeYmin = new CANNON.Body({ mass: 0, material: stone });
    planeYmin.addShape(planeShapeYmin);
    planeYmin.quaternion.setFromAxisAngle(new CANNON.Vec3(1,0,0), -Math.PI/2);
    planeYmin.position.set(0,-10,0);
    world.add(planeYmin);
    // demo.addVisual(planeYmin);
    planeYmin.addEventListener("collide", function(e){ demo.collide(e.body.id); });

    // Plane +y
    var planeShapeYmax = new CANNON.Plane();
    var planeYmax = new CANNON.Body({ mass: 0,  material: stone });
    planeYmax.addShape(planeShapeYmax);
    planeYmax.quaternion.setFromAxisAngle(new CANNON.Vec3(1,0,0), Math.PI/2);
    planeYmax.position.set(0,10,0);
    world.add(planeYmax);
    // demo.addVisual(planeYmax);
    planeYmax.addEventListener("collide", function(e){ demo.collide(e.body.id); });

    // var rotatingShape = new CANNON.Box( new CANNON.Vec3(7.5, 15, 0.5) );
    var rotatingShape = new CANNON.Box( new CANNON.Vec3(7.5, 0.5, 10) );
    var rotating = new CANNON.Body({ mass: 0, material: blade });
    rotating.addShape( rotatingShape );
    rotating.allowSleep = false;
    
    // rotating.position.set(0,0,10);
    world.add( rotating );
    demo.addVisual( rotating, 'rotating' );

    // Create spheres
    var rand = 0.1;
    var h = 0;
    var sphereShape = new CANNON.Sphere(1); // Sharing shape saves memory
    world.allowSleep = false;
    for(var i=0; i<nx; i++){
        for(var j=0; j<ny; j++){
            for(var k=0; k<nz; k++){
                var sphereBody = new CANNON.Body({ mass: 1000 });
                sphereBody.addShape(sphereShape);
                sphereBody.position.set(
                    i*2-nx*0.5 + (Math.random()-0.5)*rand,
                    j*2-ny*0.5 + (Math.random()-0.5)*rand,
                    1+k*2.1+h+(i+j)*0.0
                );
                sphereBody.allowSleep = false;
                sphereBody.sleepSpeedLimit = 1;
                sphereBody.sleepTimeLimit = 5;

                world.add(sphereBody);
                demo.addVisual( sphereBody, 'sphere' );
            }
        }
    }
}

var oscServer = new osc.Server(57121, '0.0.0.0');
oscServer.on("message", function (msg, rinfo) {
    if(msg == '/rolling')
        demo.rotate();
    if(msg == '/not-rolling')
        demo.stopRotate();
});