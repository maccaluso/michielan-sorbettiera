var world, mass, body, shape, timeStep=1/60,
	camera, scene, renderer, geometry, material, mesh,
	controls;

var SCREEN_WIDTH = window.innerWidth;
var SCREEN_HEIGHT = window.innerHeight;

initThree();
initCannon();
animate();

function initCannon() {
	world = new CANNON.World();
	world.gravity.set(0,-10,0);
	world.broadphase = new CANNON.NaiveBroadphase();
	world.solver.iterations = 10;

	shape = new CANNON.Box(new CANNON.Vec3(1,1,1));
	
	mass = 1;
	body = new CANNON.Body({
		mass: 1
	});
	body.addShape(shape);
	body.angularVelocity.set(0,1,0);
	body.angularDamping = 0.5;

	world.addBody(body);
}

function initThree() {
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera( 75, SCREEN_WIDTH / SCREEN_HEIGHT, .1, 1000 );
	camera.position.z = 10;
	camera.position.y = 1;
	scene.add( camera );

	geometry = new THREE.BoxGeometry( 5, 10, .5 );
	material = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } );
	mesh = new THREE.Mesh( geometry, material );
	scene.add( mesh );

	renderer = new THREE.WebGLRenderer();
	renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );

	document.body.appendChild( renderer.domElement );

	// Trackball controls
    controls = new THREE.TrackballControls( camera, renderer.domElement );
    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.2;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = false;
    controls.dynamicDampingFactor = 0.3;
    var radius = 100;
    controls.minDistance = 0.0;
    controls.maxDistance = radius * 1000;
    //controls.keys = [ 65, 83, 68 ]; // [ rotateKey, zoomKey, panKey ]
    controls.screen.width = SCREEN_WIDTH;
    controls.screen.height = SCREEN_HEIGHT;
}

function animate() {
	requestAnimationFrame( animate );
	updatePhysics();
	render();
}

function updatePhysics() {
	// Step the physics world
	world.step(timeStep);
	// Copy coordinates from Cannon.js to Three.js
	mesh.position.copy(body.position);
	mesh.quaternion.copy(body.quaternion);
}

function render() {
	controls.update();
	renderer.render( scene, camera );
}